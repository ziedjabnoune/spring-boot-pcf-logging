# Spring Boot Logging on Pcf

## The CloudIDE

1. This repository has been authored and tested using Cloud9 IDE as PoC for an isolated environment
2. All the dependencies have been installed and encapsulated in the setup.sh script

## Development Setup on CentOS (tested on Cloud9 IDE)

1. Clone the repository
1. `cd` into the repository folder
1. Run the setup script:
```
chmod +x ./setup.sh && \
./setup.sh
```

## To run the service
```
./mvnw spring-boot:run
```

## To package the service:
```
./mvnw clean package
```

## To run the jar:
```
java -jar ./target/spring-boot-pcf-logging-0.1.0.jar
```

## To test the end-point
```
curl http://localhost:8080/greeting
curl http://localhost:8080/greeting?name=User
```

## To deploy service to pcf:
```
cf login -a api.run.pivotal.io
cf push -n spring-boot-pcf-logging -p ./target/spring-boot-pcf-logging-0.1.0.jar   
```

## Where do I rename the application

1. In `./manifest.yml` there is a line: `- name: spring-boot-pcf-logging`
2. In `./pom.xml` there is another line: `<artifactId>spring-boot-pcf-logging</artifactId>`

## Application Logging

1. In `pom.xml` `<artifactId>spring-boot-starter-web</artifactId>` installs components required for logging
2. An additional dependency in `pom.xml` `<artifactId>lombok</artifactId>` adds a capability for adding `@Slf4j` annotation to a [class](./src/main/java/hello/GreetingController.java) to streamline logging
3. `import lombok.extern.slf4j.Slf4j;` needs to be added to the import statements
3. Logging is now is as easy as: `log.info("Greeting controller called with name: {}", name);`
3. To add arbitrary fields to the log output:
```
  HashMap<String,String> paramatersMap = new HashMap<>();
	paramatersMap.put("paramater3", "World!");
  log.info(append("paramater1", "Hello").and(append("paramater2", paramatersMap)), "Greeting controller called with name: {}", name);
```
4. What to log:
 * Exceptions at ERROR level
 * Details of a request at INFO level
 * There is no practical way to describe all scenarios with one set of rules
5. `RequestLogger` logs a complete request when logging level is set to `DEBUG`
6. To set logging level locally manipulate `./application.properties`
7. To control logging level on Pcf manipulate `./src/resources/application.yml` 
8. Request logging can also be controlled using xml configuration: https://www.javadevjournal.com/spring/log-incoming-requests-spring/

## Additional Considerations

1. Correlation Id, probabaly, should be added to the logs
2. Hostname logging maybe be meaningless in the PaaS environment


## Logs Aggregation

1. Logs on Pcf can be accessed via a [command line](https://docs.cloudfoundry.org/devguide/deploy-apps/streaming-logs.html) in real-time
2. For a more robust aggregation, logs should be shipped to an external aggregator. See details documented [here](https://royalbank.sharepoint.com/teams/Omni/_layouts/15/WopiFrame.aspx?sourcedoc={74a74b32-1cd3-443e-a682-623dbf8b6613}&action=edit&wdLOR=&wd=target%28Untitled%20Section.one%7C5a75994f-93a5-4132-b772-20f68451f544%2FLogs%20Aggregation%7Cd0dce67a-14eb-4501-8762-907da2256ea3%2F%29&wdorigin=703).

## Sync vs. Async Logging

1. Async logging to file is 3.7x faster: https://blog.takipi.com/how-to-instantly-improve-your-java-logging-with-7-logback-tweaks/
2. I was not able to find a similar documented difference in peformance using stdout
